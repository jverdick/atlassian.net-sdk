﻿1. Install the Atlassian Plugin SDK 3.8
	Follow instructions here: http://confluence.atlassian.com/display/DEVNET/Setting+up+your+Plugin+Development+Environment

2. Start Jira Server
	Run "Atlassian.Jira.Test.Integration.Setup\bin\Debug\JiraSetup.exe" from an elevated command prompt
	A JIRA server will start on a separate window
	When JIRA is up test data will be loaded

3. Run the unit tests in this project